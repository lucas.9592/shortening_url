package main

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestShortenURL(t *testing.T) {
	router := setupRouter()

	payload := []byte(`{"destination": "https://www.example.com"}`)
	req, _ := http.NewRequest("PUT", "/shorten", bytes.NewBuffer(payload))
	req.Header.Set("Content-Type", "application/json")

	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
}

func TestRedirectURL(t *testing.T) {
	router := setupRouter()

	urlStore["example"] = "https://www.example.com"

	req, _ := http.NewRequest("GET", "/example", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusMovedPermanently, w.Code)
}

func setupRouter() *gin.Engine {
	router := gin.Default()
	router.PUT("/shorten", shortenURL)
	router.GET("/:shortcode", redirectURL)
	return router
}
