package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func redirectURL(c *gin.Context) {
	shortcode := c.Param("shortcode")

	mutex.Lock()
	destination, exists := urlStore[shortcode]
	mutex.Unlock()

	if !exists {
		c.JSON(http.StatusNotFound, gin.H{"error": "URL not found"})
		return
	}

	c.Redirect(http.StatusMovedPermanently, destination)
}
