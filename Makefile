.PHONY: install test

default: install

install:
	go run .

test:
	go test
