package main

import (
	"crypto/sha1"
	"encoding/base64"
	"net/http"

	"github.com/gin-gonic/gin"
)

func shortenURL(c *gin.Context) {
	var request URLRequest

	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request"})
		return
	}

	shortcode := generateShortcode(request.Destination)

	mutex.Lock()
	urlStore[shortcode] = request.Destination
	mutex.Unlock()

	shortenedURL := c.Request.Host + "/" + shortcode
	c.JSON(http.StatusOK, URLResponse{ShortenedURL: shortenedURL})
}

func generateShortcode(url string) string {
	hasher := sha1.New()
	hasher.Write([]byte(url))
	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	return sha[:8]
}
