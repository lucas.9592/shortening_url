package main

type URLRequest struct {
	Destination string `json:"destination" binding:"required"`
}

type URLResponse struct {
	ShortenedURL string `json:"shortened_url"`
}
