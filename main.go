package main

import (
	"sync"

	"github.com/gin-gonic/gin"
)

var (
	urlStore = make(map[string]string)
	mutex    sync.Mutex
)

func main() {
	router := gin.Default()

	// Endpoint to shorten the URL
	router.PUT("/shorten", shortenURL)

	// Endpoint to redirect using the shortened URL
	router.GET("/:shortcode", redirectURL)

	// Start the server
	router.Run(":8080")
}
