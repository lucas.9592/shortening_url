# shortening_url

This project is a simple URL shortening service built with Golang and the Gin Gonic framework.

## Requirements

- Go 1.16 or higher
- Gin Gonic

## Endpoints

### `PUT /shorten`

Submit a URL to be shortened.

**Request:**
```json
{
  "destination": "valid url"
}
```

**Response:**
```json
{
  "shortened_url": "shortened URL"
}
```

## How to test the app

First you need to start the server using go commands or using make

```go
go run .
```

or

```makefile
make install
```

### Using Postman:

- Open Postman and create a new request.
- Set the request type to PUT.
- Enter the URL: http://localhost:8080/shorten.
- Go to the Body tab, select raw, and choose JSON.
- Enter the JSON payload, for example:
```json
{
  "destination": "https://www.example.com"
}
```
- Click Send.
- You should see a response with the shortened URL.

### Using cURL:

- Open a terminal and use the following cURL command:
```bash
curl -X PUT http://localhost:8080/shorten -H "Content-Type: application/json" -d '{"destination": "https://www.example.com"}'
```

## How to execute unit test

```go
go test
```

or

```makefile
make test
```

You can expect an output like this:

```
lucas@Blackhole  📂 C:\Users\lucas\go\src\shortening_url  make test   
go test
[GIN-debug] [WARNING] Creating an Engine instance with the Logger and Recovery middleware already attached.

[GIN-debug] [WARNING] Running in "debug" mode. Switch to "release" mode in production.
 - using env:   export GIN_MODE=release
 - using code:  gin.SetMode(gin.ReleaseMode)

[GIN-debug] PUT    /shorten                  --> shortening_url.shortenURL (3 handlers)
[GIN-debug] GET    /:shortcode               --> shortening_url.redirectURL (3 handlers)
[GIN] 2024/06/17 - 18:56:14 | 200 |      1.7631ms |                 | PUT      "/shorten"
[GIN-debug] [WARNING] Creating an Engine instance with the Logger and Recovery middleware already attached.

[GIN-debug] [WARNING] Running in "debug" mode. Switch to "release" mode in production.
 - using env:   export GIN_MODE=release
 - using code:  gin.SetMode(gin.ReleaseMode)

[GIN-debug] PUT    /shorten                  --> shortening_url.shortenURL (3 handlers)
[GIN-debug] GET    /:shortcode               --> shortening_url.redirectURL (3 handlers)
[GIN] 2024/06/17 - 18:56:14 | 301 |            0s |                 | GET      "/example"
PASS
ok      shortening_url  0.249s
```

The "PASS ok" at the end means the tests are working fine.